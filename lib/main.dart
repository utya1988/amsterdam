import 'dart:convert';

import 'package:amsterdam/FadeRoute.dart';
import 'package:amsterdam/appbar.dart';
import 'package:amsterdam/numberNaryad.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:io' show Platform;
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:http/http.dart' as http;
import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:provider/provider.dart';

import 'ChoiceReason.dart';
import 'ChoiceState.dart';
import 'Model/StateMachine.dart';
import 'MqttStatus.dart';
import 'SerialData.dart';
import 'StatusMachine.dart';
import 'package:flutter/services.dart';
import 'package:mqtt_client/mqtt_client.dart';

import 'constants.dart';
MqttServerClient client =
MqttServerClient.withPort(Constants.serverUri, 'flutter_client', Constants.port);
String titleTest="";


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());

}

class MyApp extends StatelessWidget {


  static const color1 = const Color(0x455066);
  static const PrimaryColor = const Color(0xff455066);

  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
    _checkPlatform();
    connect(context);
    return ChangeNotifierProvider<SerialData>(
      create: (_)=>SerialData(),
      child: MaterialApp(
        navigatorKey: NavigationService.navigatorKey,
        title: 'Flutter Demo',
        theme: ThemeData(
            backgroundColor: Color(0xffd2e8fd),
            primaryColor: MyApp.PrimaryColor),
        initialRoute: '/',
        routes: {
          '/': (context) => StatusScreen(),
          //'/choice': (context) => ChoiceMachine(),
          '/choiceState': (context) => ChoiceState(),
          '/naryad': (context) => Naryad(),
          '/choiceReason': (context) => ChoiceReason(),
          '/statusMachine': (context) => StatusMachine(text: '',)
        },
      ),
    );
  }

  void _checkPlatform() {
    if (kIsWeb) {
      print("Web");
    } else {
      if (Platform.isAndroid) {
        print("Android");
      } else if (Platform.isIOS) {
        print("Apple");
      }
    }
  }
}

class StatusScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var appbarHeight = MediaQuery.of(context).size.height * 0.1;
    return Scaffold(
      backgroundColor: Color(0xffd2e8fd),
      //appBar: AppBar(),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(appbarHeight),
        child: CustomAppBar('time'),
      ),
      body: Center(
        child: FlatButton(
          child: GridofStatnok(context),
          onPressed: () {
            //Navigator.push(context, FadeRoute(page: StatusMachine()));
          },
        ),
      ),
    );
  }
}


Widget GridofStatnok(BuildContext context) {

  Future<List> fetchAds() async {
    final response = await http.get(
        Uri.parse('https://jsonplaceholder.typicode.com/albums'));

    return context.watch<SerialData>().getData1;
    if (response.statusCode == 200)
      return json.decode(response.body);
    return [];
  }

  List<String> listStanok = ['Подналадка', 'Нет наладчика', 'Нет оператора',
    'Замена бухты', 'Поломка','Ремонт','Нет задания', 'Нет оснастки',
    'Нет материала','Подготовка станка к наладке','Тех. перерыв'];


  return MediaQuery.removePadding(
    context: context,
    removeTop: true,
    child: GridView.builder(
        itemCount: context.watch<SerialData>().getNumber,
        scrollDirection: Axis.horizontal,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisSpacing: 10, crossAxisCount: 2),
        itemBuilder: (context, index) {
          return PlanetRow(textLabel: context.watch<SerialData>().getData1[index]["time"].toString(),
            colorCard:context.watch<SerialData>().getData1[index]["state"].toString(),
            idStanka:context.watch<SerialData>().getData1[index]["idStannka"].toString(),
            person: context.watch<SerialData>().getData1[index]["persion"].toString(),
          );
        })
  );
}


class PlanetRow extends StatelessWidget {


  const PlanetRow({Key? key, required this.textLabel,
    required this.colorCard,
    required this.idStanka,
    required this.person}) : super(key: key);
  final String idStanka;
  final String person;
  final String textLabel;
  final String colorCard;



  @override
  Widget build(BuildContext context) {

    return new Container(
        height: 120.0,
        margin: const EdgeInsets.symmetric(
          vertical: 16.0,
          horizontal: 16.0,
        ),
        child: new Stack(
          children: <Widget>[
        new Container(
        height: 120.0,
          width: 200,
          margin: new EdgeInsets.only(left: 46),
          decoration: new BoxDecoration(
            color: datectColor(colorCard),
            shape: BoxShape.rectangle,
            borderRadius: new BorderRadius.circular(8.0),
            boxShadow: <BoxShadow>[
              new BoxShadow(
                color: Colors.black12,
                blurRadius: 10.0,
                offset: new Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Center(
              child: RichText(
                text: TextSpan(
                    style: TextStyle(fontSize: 20,
                      color: Colors.white),
                  children: [
                    TextSpan(text: idStanka + "\n"),
                    TextSpan(text: textLabel+ "\n"),
                    TextSpan(text: person+ "\n"),
                  ]

              ),
                textAlign: TextAlign.center,)

          ),
        ), InkWell(
              onTap: (){
                print(idStanka+textLabel);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => StatusMachine(text: idStanka)));
                //Navigator.push(context, FadeRoute(page: StatusMachine()));
              },
            )
          ],
        ));
  }
}

datectColor(String Color )  {
  if (Color=="Работа"){
    return Colors.green;
  }
  else if (Color=="Простой"){
    return Colors.redAccent;
  }
  else if (Color=="Наладка"){
    return Colors.blueAccent;
  }
  else {
    return Colors.amberAccent;
  }
}

datectState(int Color )  {
  if (Color==1){
    return Colors.green;
  }
  else if (Color==2){
    return Colors.redAccent;
  }
  else if (Color==3){
    return Colors.blueAccent;
  }
  else {
    return Colors.amberAccent;
  }
}

Future<MqttServerClient> connect(BuildContext context) async {
  client.logging(on: false);
  client.onConnected = onConnected;
  client.onDisconnected = onDisconnected;
  //client.onUnsubscribed = onUnsubscribed;
  client.onSubscribed = onSubscribed;
  client.onSubscribeFail = onSubscribeFail;
  client.pongCallback = pong;

  final connMessage = MqttConnectMessage()
      .authenticateAs(Constants.username, Constants.password)
      .keepAliveFor(60)
      .withWillTopic('willtopic')
      .withWillMessage('Will message')
      .startClean()
      .withWillQos(MqttQos.atLeastOnce);
  client.connectionMessage = connMessage;
  try {
    await client.connect();
  } catch (e) {
    print('Exception: $e');
    client.disconnect();
  }

  client.updates!.listen((List<MqttReceivedMessage<MqttMessage>> c) {
    final message = c[0].payload as MqttPublishMessage;
    final payload =
    MqttPublishPayload.bytesToStringAsString(message.payload.message);
    //
    // print('Received message:$payload from topic: ${c[0].topic}>');
    List<dynamic> responseJson = json.decode(utf8.decode(message.payload.message));
    print(responseJson.length);
    print(responseJson[0]["state"]);
    titleTest=responseJson[0]["state"];

    NavigationService.navigatorKey.currentContext!.
    read<SerialData>().changeString(responseJson);

    //context.read<SerialData>().changeString(responseJson[0]["state"]);
  });

  return client;
}


// connection succeeded
void onConnected() {
  print('Connected');
  client.subscribe("torsion/machinestate", MqttQos.atLeastOnce);
}

// unconnected
void onDisconnected() {
  print('Disconnected');
  client.unsubscribe('torsion/machinestate');
}

// subscribe to topic succeeded
void onSubscribed(String topic) {
  print('Subscribed topic: $topic');
}

// subscribe to topic failed
void onSubscribeFail(String topic) {
  print('Failed to subscribe $topic');
}

// unsubscribe succeeded
void onUnsubscribed(String topic) {
  print('Unsubscribed topic: $topic');
}

// PING response received
void pong() {
  print('Ping response client callback invoked');
}


class NavigationService {
  static GlobalKey<NavigatorState> navigatorKey =
  GlobalKey<NavigatorState>();
}