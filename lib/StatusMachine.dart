

import 'package:amsterdam/ChoiceState.dart';
import 'package:flutter/material.dart';

import 'FadeRoute.dart';
import 'appbar.dart';
import 'main.dart';

class StatusMachine extends StatelessWidget {


  // receive data from the FirstScreen as a parameter
  StatusMachine({Key? key, required this.text}) : super(key: key);
  final String text;

  @override
  Widget build(BuildContext context) {
    var iconsize=MediaQuery.of(context).size.height*0.2;
    var fontsize=40* MediaQuery.textScaleFactorOf(context);
    var appbarHeight=MediaQuery.of(context).size.height*0.1;
    return Scaffold(
        backgroundColor: Color(0xffd2e8fd),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(appbarHeight),
          child: CustomAppBar('time'),
        ),
        body: Container(
          margin: EdgeInsets.only(
            //top: MediaQuery.of(context).size.height*0.01,
            left:MediaQuery.of(context).size.width*0.05,
            right:MediaQuery.of(context).size.width*0.05,
            //bottom: MediaQuery.of(context).size.height*0.01,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              new Padding(
                padding: EdgeInsets.only(
                    left:MediaQuery.of(context).size.width*0.01),
              ),
              new Container(
                padding: EdgeInsets.only(
                    top:MediaQuery.of(context).size.width*0.01,
                    bottom:MediaQuery.of(context).size.width*0.01,
                    left:MediaQuery.of(context).size.width*0.01,
                    right:MediaQuery.of(context).size.width*0.03,

                ),
                margin: EdgeInsets.only(
                  top:MediaQuery.of(context).size.width*0.01,
                  bottom:MediaQuery.of(context).size.width*0.01,
                  left:MediaQuery.of(context).size.width*0.01,
                  right:MediaQuery.of(context).size.width*0.05,

                  //bottom:MediaQuery.of(context).size.width*0.01,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(3),
                  color: Colors.green,
                ),
              ),
              new Padding(
                padding: EdgeInsets.only(
                    left:MediaQuery.of(context).size.width*0.01),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                      text: 'Наладчик ',
                      style: TextStyle(fontSize: fontsize, color: Colors.black),
                      children:  <TextSpan>[
                        TextSpan(text: text, style: TextStyle(fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ),
                  FittedBox(
                    child: Text("Простой",style: TextStyle(
                        fontSize: fontsize)),
                  ),
                  FittedBox(
                    child: Text("12:03:31",style: TextStyle(
                        fontSize: fontsize),),
                  ),
                  FittedBox(
                    child: Text("Площадка станка",style: TextStyle(
                        fontSize: fontsize)),
                  ),
                  FittedBox(
                    child: Text("Терминал Заблокирован",style: TextStyle(
                        fontSize: fontsize)),
                  ),
                ],
              ),
              new Padding(
                padding: EdgeInsets.only(
                    left:MediaQuery.of(context).size.width*0.01),
              ),
              IconButton(
                iconSize: iconsize,
                icon: Image.asset('faceID_icon.png'),
                onPressed: () {
                  Navigator.push(context, FadeRoute(page: ChoiceState()));
                },
              ),
              new Padding(
                padding: EdgeInsets.only(
                    left:MediaQuery.of(context).size.width*0.01),
              ),
              // IconButton(
              //   iconSize: iconsize,
              //   icon: new Icon(Icons.bluetooth_audio_sharp,
              //     color: Colors.blueAccent,),
              //   onPressed: () {  },
              // ),
              // new Padding(
              //   padding: EdgeInsets.only(
              //       left:MediaQuery.of(context).size.width*0.01),
              // ),
              // IconButton(
              //   iconSize: iconsize,
              //   icon: Image.asset('nfc-icon-26.jpg'),
              //   onPressed: () {  },
              // ),
              new Padding(
                padding: EdgeInsets.only(
                    right:MediaQuery.of(context).size.width*0.01),
              ),
            ],
          ),
        ));
  }
}
