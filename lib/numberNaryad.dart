import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_grid_button/flutter_grid_button.dart';

import 'FadeRoute.dart';
import 'main.dart';

class Naryad extends StatelessWidget {

  final myController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var txtSize=MediaQuery.of(context).size.height*0.1;
    return Scaffold(
        backgroundColor: Color(0xffd2e8fd),
          body: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.05),
                padding: EdgeInsets.only(top:MediaQuery.of(context).size.height*0.01,
                    left:MediaQuery.of(context).size.width*0.20,
                    right:MediaQuery.of(context).size.width*0.20),
                child: Container(
                  height: MediaQuery.of(context).size.height*0.15,
                  child: TextFormField(
                    controller: myController,
                    textAlign: TextAlign.center,
                    cursorColor: Colors.transparent,
                    cursorWidth: 0,
                    style: TextStyle(
                      fontSize: MediaQuery.of(context).size.height*0.1,
                    ),
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                    decoration: InputDecoration(
                      fillColor: Colors.white,
                        filled: true,
                        border: OutlineInputBorder(),
                        hintText: 'Номер наряда'),
                  ),
                ),
              ),

               Padding(
                 padding: EdgeInsets.only(
                     top:MediaQuery.of(context).size.height*0.01,
                     bottom:MediaQuery.of(context).size.height*0.01,
                     left:MediaQuery.of(context).size.width*0.20,
                     right:MediaQuery.of(context).size.width*0.20),
                 child: SizedBox(
                   height: MediaQuery.of(context).size.height*0.70,
                  child: GridButton(
                      items: [
                        [
                          GridButtonItem(title: "1",
                              color: new Color(0xFF333366),
                              textStyle: TextStyle(
                              fontSize: txtSize,
                              color: Colors.white
                          )),
                          GridButtonItem(title: "2",
                              color: new Color(0xFF333366),
                              textStyle: TextStyle(
                                  fontSize: txtSize,
                                  color: Colors.white
                              )),
                          GridButtonItem(title: "3",
                              color: new Color(0xFF333366),
                              textStyle: TextStyle(
                                  fontSize: txtSize, color: Colors.white
                              )),
                        ],
                        [
                          GridButtonItem(title: "4",color: new Color(0xFF333366),
                              textStyle: TextStyle(
                                  fontSize: txtSize, color: Colors.white
                              )),
                          GridButtonItem(title: "5",color: new Color(0xFF333366),
                              textStyle: TextStyle(
                                  fontSize: txtSize, color: Colors.white
                              )),
                          GridButtonItem(title: "6",color: new Color(0xFF333366),
                              textStyle: TextStyle(
                                  fontSize: txtSize, color: Colors.white
                              )),
                        ],
                        [
                          GridButtonItem(title: "7",color: new Color(0xFF333366),
                              textStyle: TextStyle(
                                  fontSize: txtSize, color: Colors.white
                              )),
                          GridButtonItem(title: "8",color: new Color(0xFF333366),
                              textStyle: TextStyle(
                                  fontSize: txtSize, color: Colors.white
                              )),
                          GridButtonItem(title: "9",color: new Color(0xFF333366),
                              textStyle: TextStyle(
                                  fontSize: txtSize, color: Colors.white
                              )),
                        ],
                        [
                          GridButtonItem(title: "",
                              value: "Del",
                              color: Colors.redAccent,
                              child: Icon(
                                Icons.backspace,
                                size: MediaQuery.of(context).size.height*0.1,
                              ),
                              textStyle: TextStyle(
                                  fontSize: txtSize, color: Colors.white
                              )),
                          GridButtonItem(title: "0",color: new Color(0xFF333366),
                              textStyle: TextStyle(
                                  fontSize: txtSize, color: Colors.white
                              )),
                          GridButtonItem(title: "",
                              value: "Ok",
                              child: Icon(
                                Icons.check,
                                size: MediaQuery.of(context).size.height*0.1,
                              ),
                              color: Colors.green,
                              textStyle: TextStyle(
                                  fontSize: txtSize, color: Colors.white
                              )),
                        ],
                      ], onPressed: (value) {
                        var datainfield =myController.text;
                        if (value=="Del"){
                        datainfield=_removeLastCharacter(datainfield);
                        myController.text=datainfield;
                        }else if (value=="Ok"){
                          Navigator.push(context, FadeRoute(page: StatusScreen()));
                        } else{
                          myController.text=datainfield+value;
                        }},
                    ),
              ),
               ),
            ],
          ),
        );
  }

  _getnumbers(String value){
    var datainfield =myController.text;
    myController.text=datainfield+value;


  }
}

_removeLastCharacter(String str) {
  if (str.length > 0) {
    str = str.substring(0, str.length - 1);
  }
  return str;
}





