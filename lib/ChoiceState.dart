import 'package:amsterdam/main.dart';
import 'package:amsterdam/numberNaryad.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'ChoiceReason.dart';
import 'FadeRoute.dart';

// ignore: must_be_immutable
class ChoiceState extends StatelessWidget {
  //can change size button and text

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffd2e8fd),
      // appBar: PreferredSize(
      //   preferredSize: const Size.fromHeight(50),
      //   child: CustomAppBar('time'),
      // ),
      body: _getButtons(context),
    );
  }

  _getButtons(BuildContext context) {
    var txtSize = MediaQuery.of(context).size.height*0.1;
    var scaleHeightButton=0.08;
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(

              left:MediaQuery.of(context).size.width*0.20,
              right:MediaQuery.of(context).size.width*0.20,
          ),
          child: SizedBox
            (
            width: double.infinity,
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(context, FadeRoute(page: StatusScreen()));
              },
              child: Text(
                "Работа",
                style: TextStyle(
                  fontSize: txtSize,
                ),
              ),
              style: ElevatedButton.styleFrom(
                  primary: Colors.green,
                  padding: EdgeInsets.all(MediaQuery.of(context).size.height*scaleHeightButton),
                  //minimumSize: Size(btnHeight, btnWeight)
              ),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            left:MediaQuery.of(context).size.width*0.20,
            right:MediaQuery.of(context).size.width*0.20,
    ),
          alignment: Alignment.center,
          child: SizedBox(
            width: double.infinity,
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(context, FadeRoute(page: Naryad()));
              },
              child: Text(
                "Наладка",
                style: TextStyle(
                  fontSize: txtSize,
                ),
              ),
              style: ElevatedButton.styleFrom(
                  primary: Colors.blue,
                  padding: EdgeInsets.all(MediaQuery.of(context).size.height*scaleHeightButton),
                  //minimumSize: Size(btnHeight, btnWeight)
              ),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(
              //top: MediaQuery.of(context).size.height*0.01,
              left:MediaQuery.of(context).size.width*0.20,
              right:MediaQuery.of(context).size.width*0.20,
              //bottom: MediaQuery.of(context).size.width*0.01
          ),
          alignment: Alignment.center,
          child: SizedBox(
            width: double.infinity,
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(context, FadeRoute(page: ChoiceReason()));
              },
              child: Text(
                "Простой",
                style: TextStyle(
                  fontSize: txtSize,
                ),
              ),
              style: ElevatedButton.styleFrom(
                  primary: Colors.redAccent,
                  padding: EdgeInsets.all(MediaQuery.of(context).size.height*scaleHeightButton)
              ),
            ),
          ),
        ),
      ],
    );
  }
}
