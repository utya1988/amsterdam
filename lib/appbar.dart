import 'package:flutter/material.dart';
import 'dart:async';
import 'package:intl/intl.dart';


class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {

  final String timeStringInput;
  CustomAppBar(this.timeStringInput) : preferredSize = Size.fromHeight(kToolbarHeight);

  @override
  final Size preferredSize; // default is 56.0


  @override
  _CustomAppBarState createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar>{

  String _timeString="timeStringInput";



  @override
  void initState() {
    super.initState();
    Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
  }

  @override
  Widget build(BuildContext context) {
    //_timeString=widget.timeStringInput;
    var fontsize=MediaQuery.of(context).size.height*0.05;
    _getTime();
    return AppBar(
      title: Padding(
        padding: EdgeInsets.fromLTRB(0.0,
            MediaQuery.of(context).size.height*0.02,
            MediaQuery.of(context).size.height*0.02,
            0.0),
        child: Text("Смена №2", style:TextStyle(fontSize: fontsize, fontWeight: FontWeight.bold)),
      ),
      centerTitle: false,

      actions: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(0.0,
              MediaQuery.of(context).size.height*0.02,
              MediaQuery.of(context).size.height*0.02, 0.0),
          child: GestureDetector(
            onTap: (){
            },
            child: Text(_timeString.toString(), style: TextStyle(fontSize: fontsize, fontWeight: FontWeight.bold)),
          ),)
      ],
      automaticallyImplyLeading: false, );
  }


  void _getTime() {
    final String formattedDateTime = DateFormat('yyyy-MM-dd kk:mm:ss').format(DateTime.now()).toString();
    setState(() {
      _timeString = formattedDateTime;
    });
  }

  void _checkSmena() {
    final String formattedDateTime = DateFormat('yyyy-MM-dd kk:mm:ss').format(DateTime.now()).toString();
    setState(() {
      _timeString = formattedDateTime;
    });
  }
}