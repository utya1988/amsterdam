import 'dart:convert';

import 'package:flutter/widgets.dart';

class SerialData with ChangeNotifier{
  String _data= "Test string";
  int _numberMachine=0;
  late List<dynamic> _data1;

  String get getData=>_data;
  List<dynamic> get getData1=>_data1;
  int get getNumber=>_numberMachine;

  void changeString(List<dynamic> newString){
    _numberMachine=newString.length;
    _data=newString[0]["state"];
    _data1=newString;
    print(newString);
    notifyListeners();
  }
}