import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'FadeRoute.dart';
import 'appbar.dart';
import 'main.dart';

class ChoiceReason extends StatelessWidget {
  final _controller = ScrollController();
  final _height = 300.0;
  final _width = 100.0;
  var listReason = ['Подналадка', 'Нет наладчика', 'Нет оператора',
  'Замена бухты', 'Поломка','Ремонт','Нет задания', 'Нет оснастки',
  'Нет материала','Подготовка станка к наладке','Тех. перерыв'];

  @override
  Widget build(BuildContext context) {

    var appbarHeight = MediaQuery.of(context).size.height * 0.1;
    final orientation = MediaQuery.of(context).orientation;
    var txtSize=MediaQuery.of(context).size.height*0.07;

    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - 35) / 2;
    final double itemWidth = size.width / 1;
    return Scaffold(
      backgroundColor: Color(0xffd2e8fd),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(appbarHeight),
        child: CustomAppBar('time'),
      ),
      floatingActionButton: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 120),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: const EdgeInsets.all(1.0),
                child: SizedBox(
                  height: 70,
                  width: 200,
                  child: ElevatedButton(
                    onPressed: () => _animateToIndex(0),
                    child: Icon(Icons.arrow_back),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 40, right: 100),
            child: Align(
              alignment: Alignment.bottomRight,
              child: Padding(
                padding: const EdgeInsets.all(1.0),
                child: SizedBox(
                  height: 70,
                  width: 200,
                  child: ElevatedButton(
                    onPressed: () => _animateToIndex(listReason.length),
                    child: Icon(Icons.arrow_forward),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
      body:
          //padding: const EdgeInsets.only(bottom: 150),
          Padding(
        padding: const EdgeInsets.only(bottom: 90.0),
        child: GridView.builder(
          controller: _controller,
          scrollDirection: Axis.horizontal,
          itemCount: listReason.length,
          itemBuilder: (listReason1, i) => Container(
              height: _height,
              width: _width,
              child: Card(
                color: new Color(0xFF333366),
                child: InkWell(
                    onTap: () {
                      Navigator.push(context, FadeRoute(page: StatusScreen()));
                    },
                    child: Center(child: Text(listReason[i], textAlign: TextAlign.center,
                      style:  TextStyle(
                        fontSize: txtSize,
                        color: Colors.white
                    )
                    ))),
              )),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3, childAspectRatio: (itemHeight / itemWidth)),
        ),
      ),
    );
  }

  _animateToIndex(i) => _controller.animateTo(_width * i,
      duration: Duration(milliseconds: 500), curve: Curves.fastOutSlowIn);
}
