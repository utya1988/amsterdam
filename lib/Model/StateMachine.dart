// To parse this JSON data, do
//
//     final modelState = modelStateFromJson(jsonString);

import 'dart:convert';

List<ModelState> modelStateFromJson(String str) => List<ModelState>.from(json.decode(str).map((x) => ModelState.fromJson(x)));

String modelStateToJson(List<ModelState> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ModelState {
  ModelState({
    required this.idStannka,
    required this.time,
    required this.state,
    required this.reason,
    required this.persion,
  });

  int idStannka;
  String time;
  String state;
  String reason;
  int persion;

  factory ModelState.fromJson(Map<String, dynamic> json) => ModelState(
    idStannka: json["idStannka"] == null ? null : json["idStannka"],
    time: json["time"] == null ? null : json["time"],
    state: json["state"] == null ? null : json["state"],
    reason: json["reason"] == null ? null : json["reason"],
    persion: json["persion"] == null ? null : json["persion"],
  );

  Map<String, dynamic> toJson() => {
    "idStannka": idStannka == null ? null : idStannka,
    "time": time == null ? null : time,
    "state": state == null ? null : state,
    "reason": reason == null ? null : reason,
    "persion": persion == null ? null : persion,
  };
}
